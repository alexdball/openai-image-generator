#!/usr/bin/env python3
###################################################################
#
#	File    : GenerateImage.py
#	Author  : adb <alex@alexdball.dev>
#	Created : 23-02-18
#
###################################################################
#
#
###################################################################
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.
###################################################################
#
# Import required libraries / modules
import os
from datetime import datetime as dt
import requests
import json
import sys

# Image Configuration
IMG_SAVE_PATH       = "images/"                                                                                           # Set the path to save generated images

# API Configuration
API_BASE_URL        = "https://api.openai.com/v1"
API_KEY             = os.getenv('OPENAI_API_KEY')                                                                       # Get the API Key from environment variable (for security)


def generate_image(img_description,img_count):

    ''' Generates an image based on the given description '''

    # Check if a valid description was given
    if img_description is None or len(img_description) == 0: 
        print("[!] Please provide a description for the image to be generated.. Aborting")
        exit()

    try:                                                                                                                # Check if a valid number was provided for image count
        image_count = int(img_count)
    except:
        print("[!] Invalid image count detected. Defaulting to 1")                                                      # Set image counter to 1 on error
        image_count = 1

    API_ENDPOINT = "/images/generations"                                                                                # Set the API endpoint

    headers = {                                                                                                         # Create the headers object to send to the API endpoint
        "Content-Type" : "application/json",
        "Authorization" : f"Bearer {API_KEY}"
    }

    data = json.dumps(                                                                                                  # Create the data object to send to the api endpoint
        {
            "model"             : "image-alpha-001",
            "prompt"            : img_description,
            "num_images"        : image_count,
            "size"              : "1024x1024",
            "response_format"   : "url"
        }
    )

    response = requests.post(                                                                                           # Send the request to the API endpoint
        API_BASE_URL + API_ENDPOINT,
        headers=headers,
        data=data
    )

    if response.status_code != 200:                                                                                     # Set the message in case of Request error
        error_code      = response.json()['error']['code']                                                              # Error Code
        error_message   = response.json()['error']['message']                                                           # Error Message
        print(f"[x] Request Error : \n\t==> Error Code : {error_code}\n\t==> Error Message : {error_message}")
        print("\n[x] Aborting..")

        return None                                                                                                     # Return NULL object so the application aborts

    return response.json()                                                                                              # Return the response for further handling

def ProcessImages(imageList):

    images = []                                                                                                         # Create a list to hold the image URLs

    if imageList is None or len(imageList) == 0:                                                                        # Check if a valid image list was provided and that it contains images
        print("[x] No images to process.. Aborting")
        exit()

    try:

        create_date = dt.fromtimestamp(imageList['created'])                                                            # Get the datetime for the  generated images.

        for image in imageList['data']: images.append(image['url'])                                                     # Add the image URL to the list

        return images                                                                                                   # Return the image URL list

    except:
        print("[x] Error handling images.. Aborting")
        return None


def DownloadImages(ImageUrls):
    
    if ImageUrls is None or len(ImageUrls) == 0:                                                                        # Check if valid URLS were provided
        print("[!] No URLs to process.. Aborting,,")
        exit()
        
    if IMG_SAVE_PATH is None or len(IMG_SAVE_PATH) == 0 :                                                               # Check if we have an image path set (does not check if it exists though)
        print("[x] Invalid Save Path specified..Aborting")
        return None

    img_index = 0                                                                                                       # Set the image Index
    
    for img in ImageUrls:

        try:

            img_index +=1                                                                                               # Increment the image index
            file_timestamp = int(dt.timestamp(dt.now()))                                                                # Create a timestamp for the filename
            
            dl_response = requests.get(img)                                                                             # Get the image data from the URL

            img_filename = f"{IMG_SAVE_PATH}{file_timestamp}_{img_index}.png"                                           # Set the image's filename

            print(f"==> Downloading image {img_index}/{len(ImageUrls)} to {img_filename}")

            if dl_response.status_code:                                                                                 # Check if the URL is valid
                with open(img_filename,'wb') as IMG_FILE: 
                    IMG_FILE.write(dl_response.content)                           # Write the response bytes to the file

        except:
            print("[x] There was an error downloading image(s).. Aborting")
            exit()


try:

    image_description = input("==> Enter the image description : ")                                                     # Get image description from stdin

    if len(image_description) == 0:                                                                                     # Check if a valid description was given
        print("[x] Invalid Image description. Aborting..")
        exit()

    image_count = input("==> Enter the amount of images to be generated (1-4) : ")                                      # Get the amount of images to generate

    try:
        image_count = int(image_count)                                                                                  # Convert the image input to integer value
    except:
        print("[x] Invalid number entered for image count.. Aborting..")                                                # Abort on invalid input
        exit()

    if image_count > 4:                                                                                                 # Check if amount of images is in the acceptable range
        print("[x] Number of images to be generated is too large.. Aborting..")
        exit()

    print("\n[!] You have given the following parameters for the images to be generated : ")                            # Show confirmation message
    print(f"\t==> Description : {image_description}")
    print(f"\t==> Image Count : {image_count}")

    confirm = input("\n[?] Are you sure you want to proceed ? (Y/N) : ")                                                # Get confirmation from stdin

    if not confirm.upper() == "Y": exit()                                                                               # Abort if not Y/y

    print("\n\n==> Generating images based on description...\n")
    generated_images = generate_image(image_description, image_count)                                                   # Generate the image(s)

    if generated_images is None: exit()                                                                                 # Exit application if there was an error with the request
    DownloadImages(ProcessImages(generated_images))                                                                     # Process and download the image(s)

except:
    print("\n\n[x] Aborted..")                                                                                          # General abort on error
    exit()